<?php

namespace Th20\UploadHelperBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('th20_upload_helper');

        $rootNode
            ->children()
                ->scalarNode('storage_root')->defaultValue(null)->end()
                ->scalarNode('web_root')->defaultValue(null)->end()
            ->end();

        return $treeBuilder;
    }
}
