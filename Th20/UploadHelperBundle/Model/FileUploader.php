<?php

namespace Th20\UploadHelperBundle\Model;


use InvalidArgumentException;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Th20\UploadHelperBundle\Entity\Uploadable;


class FileUploader
{

    const NAME_RANDOM = 1;

    const NAME_PRESERVE = 2;


    protected $pm;


    public function __construct(FilePathManager $pm)
    {
        $this->pathManager = $pm;
    }

    public function uploadAll(Uploadable $entity)
    {
        foreach ($entity->getUploadableFilesMapping()->getMappings() as $field => $mapping) {
            $this->_uploadField($mapping);
        }
    }

    public function uploadField(Uploadable $entity, $fileField)
    {
        $mapping = $entity->getUploadableFilesMapping()->getMapping($fileField);
        if ($mapping) {
            return $this->_uploadField($mapping);
        }
        return false;
    }

    public function storeField(Uploadable $entity, $fileField, $naming = self::NAME_RANDOM)
    {
        $mapping = $entity->getUploadableFilesMapping()->getMapping($fileField);
        if ($mapping) {
            return $this->_storeField($mapping, $naming);
        }
        return false;
    }

    protected function _uploadField(FieldMapping $mapping)
    {
        $file = $mapping->callFileFieldGetter();
        if (!($file instanceof UploadedFile)) {
            return false;
        }
        return $this->storeFile($mapping, $file, self::NAME_RANDOM);
    }

    protected function _storeField($mapping, $naming)
    {
        $file = $mapping->callFileFieldGetter();
        if (!($file instanceof File)) {
            return false;
        }
        return $this->storeFile($mapping, $file, $naming);
    }

    protected function storeFile($mapping, $file, $naming)
    {
        $originalName = $file instanceof UploadedFile
            ? $file->getClientOriginalName()
            : $file->getFilename();

        $path = $this->lookupMappingStorePath($mapping);

        switch ($naming) {
            case self::NAME_RANDOM:
                $name = $this->generateUniqueName($path, $this->guessExtension($file));
                break;

            case self::NAME_PRESERVE:
            default:
                $name = $originalName;
        }

        try {
            $newFile = $file->move($path, $name);
        } catch (FileException $e) {
            return false;
        }

        $mapping->callFileFieldSetter(null);
        $mapping->callFileFieldUploader($newFile, $path, $name, $originalName);
        return true;
    }

    protected function lookupMappingStorePath($mapping)
    {
        return $this->pathManager->lookupFieldMappingPath($mapping);
    }

    protected function generateUniqueName($path, $ext)
    {
        $name = tempnam($path, 'upl_' . time() . '_');
        unlink($name);

        $name .= '.' . $ext;
        return pathinfo($name, PATHINFO_BASENAME);
    }

    protected function guessExtension($file)
    {
        $ext = $file instanceof UploadedFile
            ? $file->getClientOriginalExtension()
            : $file->getExtension();

        if (preg_match('/^[a-z0-9]+$/i', $ext)) {
            return $ext;
        } else {
            return $file->guessExtension();
        }
    }

}
