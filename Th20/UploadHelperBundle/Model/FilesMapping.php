<?php

namespace Th20\UploadHelperBundle\Model;


use InvalidArgumentException;

use Th20\UploadHelperBundle\Entity\Uploadable;


class FilesMapping
{

    protected $entity;

    protected $mappings;


    public function __construct($entity)
    {
        if (!is_subclass_of($entity, 'Th20\UploadHelperBundle\Entity\Uploadable')) {
            throw new InvalidArgumentException('FilesMapping can be used only with instance of Uploadable.');
        }
        $this->entity = $entity;
        $this->mappings = array();
    }

    public function addMapping($fileField, $filenameField)
    {
        $mapping = new FieldMapping($this->entity);
        $mapping->setFileField($fileField);
        $mapping->setFilenameField($filenameField);

        $this->mappings[$fileField] = $mapping;
        return $mapping;
    }

    public function getMappings()
    {
        return $this->mappings;
    }

    public function getMapping($fileField)
    {
        return isset($this->mappings[$fileField]) ? $this->mappings[$fileField] : null;
    }

}
