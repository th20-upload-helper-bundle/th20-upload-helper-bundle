<?php

namespace Th20\UploadHelperBundle\Model;


use InvalidArgumentException;
use ReflectionProperty;

use Th20\UploadHelperBundle\Entity\Uploadable;


class FieldMapping
{

    protected $entity;

    protected $className;

    protected $fileField;

    protected $filenameField;

    protected $sizeField;

    protected $originalField;

    protected $pathSuffix;


    public function __construct($entity)
    {
        if (!is_subclass_of($entity, 'Th20\UploadHelperBundle\Entity\Uploadable')) {
            throw new InvalidArgumentException('FieldMapping can be used only with instance of Uploadable.');
        }
        $this->entity = $entity;
        $this->className = get_class($entity);
        $this->pathSuffix = array();
    }

    public function setFileField($field)
    {
        $this->checkFieldRequirements($field);
        $this->fileField = $field;
        return $this;
    }

    public function setFileFieldValue($value)
    {
        $reflection = new ReflectionProperty($this->entity, $this->fileField);
        $reflection->setAccessible(true);
        $reflection->setValue($this->entity, $value);
    }

    public function getFileFieldValue()
    {
        $reflection = new ReflectionProperty($this->entity, $this->fileField);
        return $reflection->getValue($value);
    }

    public function callFileFieldSetter()
    {
        if ($this->fileField) {
            $setter = 'set' . $this->fileField;
            return call_user_func_array(array($this->entity, $setter), func_get_args());
        }
        return null;
    }

    public function callFileFieldGetter()
    {
        if ($this->fileField) {
            $setter = 'get' . $this->fileField;
            return call_user_func_array(array($this->entity, $setter), func_get_args());
        }
        return null;
    }

    public function callFileFieldUploader()
    {
        if ($this->fileField) {
            $setter = 'upload' . $this->fileField;
            return call_user_func_array(array($this->entity, $setter), func_get_args());
        }
        return null;
    }

    public function setFilenameField($field)
    {
        $this->checkFieldRequirements($field);
        $this->filenameField = $field;
        return $this;
    }

    public function callFilenameFieldSetter()
    {
        if ($this->filenameField) {
            $setter = 'set' . $this->filenameField;
            return call_user_func_array(array($this->entity, $setter), func_get_args());
        }
        return null;
    }

    public function callFilenameFieldGetter()
    {
        if ($this->filenameField) {
            $setter = 'get' . $this->filenameField;
            return call_user_func_array(array($this->entity, $setter), func_get_args());
        }
        return null;
    }

    public function setSizeField($field)
    {
        $this->checkFieldRequirements($field);
        $this->sizeField = $field;
        return $this;
    }

    public function callSizeFieldSetter()
    {
        if ($this->sizeField) {
            $setter = 'set' . $this->sizeField;
            return call_user_func_array(array($this->entity, $setter), func_get_args());
        }
        return null;
    }

    public function callSizeFieldGetter()
    {
        if ($this->sizeField) {
            $setter = 'get' . $this->sizeField;
            return call_user_func_array(array($this->entity, $setter), func_get_args());
        }
        return null;
    }

    public function setOriginalField($field)
    {
        $this->checkFieldRequirements($field);
        $this->originalField = $field;
        return $this;
    }

    public function callOriginalFieldSetter()
    {
        if ($this->originalField) {
            $setter = 'set' . $this->originalField;
            return call_user_func_array(array($this->entity, $setter), func_get_args());
        }
        return null;
    }

    public function callOriginalFieldGetter()
    {
        if ($this->originalField) {
            $setter = 'get' . $this->originalField;
            return call_user_func_array(array($this->entity, $setter), func_get_args());
        }
        return null;
    }

    public function setPathSuffix(array $suffix = array())
    {
        $this->pathSuffix = $suffix;
        return $this;
    }

    public function getPathSuffix()
    {
        return $this->pathSuffix;
    }


    protected function checkFieldRequirements($field)
    {
        if (!$this->checkFieldSetter($field)) {
            $error = $this->className . '::' . $field . ' field must have a setter method.';
            throw new InvalidArgumentException($error);
        }
        if (!$this->checkFieldGetter($field)) {
            $error = $this->className . '::' . $field . ' field must have a getter method.';
            throw new InvalidArgumentException($error);
        }
    }

    protected function checkFieldSetter($field)
    {
        $name = 'set' . $field;
        return method_exists($this->entity, $name);
    }

    protected function checkFieldGetter($field)
    {
        $name = 'get' . $field;
        return method_exists($this->entity, $name);
    }

}
