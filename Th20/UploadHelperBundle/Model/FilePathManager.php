<?php

namespace Th20\UploadHelperBundle\Model;


use InvalidArgumentException;

use Symfony\Component\HttpFoundation\File\File;

use Th20\UploadHelperBundle\Entity\Uploadable;


class FilePathManager
{

    private $storageRoot;
    private $webRoot;


    public function __construct($storage, $web)
    {
        $this->storageRoot = $storage;
        $this->webRoot = $web;
    }

    public function publicPath($path)
    {
        $root = realpath($this->webRoot);
        if (strpos($path, $root) === 0) {
            return str_replace($root, '', $path);
        }
        return '';
    }

    public function resolveFieldPath($entity, $field)
    {
        $this->checkEntity($entity, $field);

        $mapping = $entity->getUploadableFilesMapping()->getMapping($field);
        if (!$mapping) {
            throw new InvalidArgumentException('Field ' . $field . ' cannot be resolved.');
        }

        $path = $this->lookupFieldMappingPath($mapping) . DIRECTORY_SEPARATOR . $mapping->callFilenameFieldGetter();
        $real = (string) realpath($path);

        if (is_file($real)) {
            return $real;
        } else {
            return $path;
        }
    }

    public function lookupFieldPath($entity, $field)
    {
        $this->checkEntity($entity, $field);

        $mapping = $entity->getUploadableFilesMapping()->getMapping($field);
        if (!$mapping) {
            throw new InvalidArgumentException('Field ' . $field . ' cannot be looked up.');
        }

        return $this->lookupFieldMappingPath($mapping);
    }

    public function lookupFieldMappingPath(FieldMapping $mapping)
    {
        $pathSuffix = $mapping->getPathSuffix();
        if (empty($pathSuffix)) {
            $pathSuffix = array('var');
        }
        if (!is_array($pathSuffix)) {
            $pathSuffix = (array) $pathSuffix;
        }

        $path = $this->storageRoot . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $pathSuffix);
        return $path;
    }

    public function getFieldFile($entity, $field)
    {
        return new File($this->resolveFieldPath($entity, $field));
    }

    public function injectFieldFile($entity, $field)
    {
        $setter = 'set' . $field;
        if (($entity instanceof Uploadable) || method_exists($entity, $setter)) {
            $entity->$setter($this->getFieldFile($entity, $field));
        }
        return $entity;
    }

    public function injectAllFieldFiles(Uploadable $entity)
    {
        foreach ($entity->getUploadableFilesMapping()->getMappings() as $field => $mapping) {
            $this->injectFieldFile($entity, $field);
        }
        return $entity;
    }

    protected function checkEntity($entity, $field)
    {
        if (is_object($entity)){
            if ($entity instanceof Uploadable) {
                return true;
            }
        }
        $error = 'Entity does not contain the specified field.';
        throw new InvalidArgumentException($error);
    }

}
