<?php

namespace Th20\UploadHelperBundle\Entity;


use BadMethodCallException;
use ReflectionProperty;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Th20\UploadHelperBundle\Model\FilesMapping;


abstract class Uploadable
{

    public function getUploadableFilesMapping()
    {
        $mapping = new FilesMapping($this);
        return $mapping;
    }

    public function __call($method, $args)
    {
        $method = strtolower($method);

        foreach ($this->getUploadableFilesMapping()->getMappings() as $field => $mapping) {
            $field = strtolower($field);

            if ($method == 'set' . $field) {
                return $this->_setFileField($mapping, $args);
            }
            if ($method == 'upload' . $field) {
                return $this->_uploadFileField($mapping, $args);
            }
        }

        throw new BadMethodCallException('Call to undefined method ' . $method);
    }


    private function _setFileField($mapping, $args)
    {
        $file = array_shift($args);

        if (!empty($file) && ($file instanceof File)) {
            $mapping->setFileFieldValue($file);
        }

        if (!empty($file) && ($file instanceof UploadedFile)) {
            // Randomize filename to erase any existent data and to make it
            // possible to pass validator checks if filename must not be empty.
            $mapping->callFilenameFieldSetter(mt_rand() . '.temp');
        }
        return $this;
    }

    private function _uploadFileField($mapping, $args)
    {
        $file = array_shift($args);
        $path = array_shift($args);
        $name = array_shift($args);
        $originalName = array_shift($args);

        $mapping->callFilenameFieldSetter($name);
        $mapping->callSizeFieldSetter($file->getSize());
        $mapping->callOriginalFieldSetter($originalName);

        return $this;
    }

}
