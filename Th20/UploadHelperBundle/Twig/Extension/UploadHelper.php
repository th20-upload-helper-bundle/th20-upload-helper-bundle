<?php

namespace Th20\UploadHelperBundle\Twig\Extension;


use Twig_Extension;
use Twig_SimpleFilter;

use Th20\UploadHelperBundle\Entity\Uploadable;
use Th20\UploadHelperBundle\Model\FilePathManager;


class UploadHelper extends Twig_Extension
{

    protected $filepathManager;


    public function __construct(FilePathManager $filepathManager)
    {
        $this->filepathManager = $filepathManager;
    }

    public function getName()
    {
        return 'upload_helper';
    }

    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('public_path', array($this, 'publicPath')),
            new Twig_SimpleFilter('filter_path', array($this, 'filterPath')),
            new Twig_SimpleFilter('load_files',  array($this, 'loadFiles')),
            new Twig_SimpleFilter('filesize_human',  array($this, 'filesizeHuman')),
        );
    }

    public function publicPath($data, $field = 'file')
    {
        if (empty($data) || !is_object($data) || !($data instanceof Uploadable)) {
            return $data;
        }

        $manager = $this->filepathManager;
        return $manager->publicPath($manager->resolveFieldPath($data, $field));
    }

    public function filterPath($data, $field = 'file')
    {
        $return = $this->publicPath($data, $field);
        if (is_string($return)) {
            $return = preg_replace('|^/?uploads|', '', $return);
        }
        return $return;
    }

    public function loadFiles($data)
    {
        if ($data instanceof Uploadable) {
            $clone = clone $data;
            return $this->filepathManager->injectAllFieldFiles($clone);
        }
        return $data;
    }

    public function filesizeHuman($size)
    {
        // This filter does not really belong to this extension, but it is a
        // waste to create a new extension for such a simple filter.

        $i = -1;
        $units = array(' kB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB');
        do {
            $size /= 1024;
            $i++;
        } while ($size > 1024);

        return round($size, 1) . $units[$i];
    }

}
